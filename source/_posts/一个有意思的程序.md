---
title: 一个有意思的程序
date: 2022-10-11 16:54:53
tags:
  - 摸鱼
categories: 
  - 摸鱼
---
## 功能介绍

输入一个  $6$ 位数，他会输出一个 $7$ 位数。

## 代码

```cpp
/*
* @Author: ftt2333
* @Email: ftt2333@126.com
* @Last Modified time: 2022-10-11 16:54:18
*/

#include<bits/stdc++.h>
using namespace std;

#define qin cin
#define qout cout
#define rep(i, a, b) for(auto i = (a); i <= (b); i++)
#define per(i, a, b) for(auto i = (a); i >= (b); i--)

char s[20];
int a[20], b[20];

int main() {
  scanf("%s", s + 1);
  rep(i, 1, 6) a[i] = s[i] - 48;
  rep(i, 1, 6) b[i * 2 - 1] = a[i], b[i * 2] = 10 - a[i];
  rep(i, 1, 4) qout << b[i];
  qout << (b[5] - b[6] + 10) % 10;
  qout << (b[7] + b[8] + b[9]) % 10;
  qout << (b[10] + b[11] + b[12] + 1) % 10;
}
```

## 彩蛋

输入`114514`有惊喜
